package com.example.amulyamummaneni.androidphotos.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.example.amulyamummaneni.androidphotos.R;

import com.example.amulyamummaneni.androidphotos.model.Album;
import com.example.amulyamummaneni.androidphotos.model.Photo;
import com.example.amulyamummaneni.androidphotos.model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class SearchActivity extends AppCompatActivity {

    private User user;
    private ArrayList<Photo> searchResults;
    private ArrayList<Photo> userPhotos;

    private GridView gridview;
    private PhotoAdapter photoAdapter;
    private Button search1button;
    private Button search2button;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        user = (User)getIntent().getSerializableExtra("User");

        searchResults = new ArrayList<>();
        userPhotos = new ArrayList<>();
        loadUserPhotos();

        photoAdapter = new PhotoAdapter(getApplicationContext(), searchResults);
        gridview = findViewById(R.id.gridview);
        gridview.setAdapter(photoAdapter);

        search1button = findViewById(R.id.search1button);
        search2button = findViewById(R.id.search2button);

        search1button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                AlertDialog.Builder builder = new AlertDialog.Builder(search1button.getContext());
                builder.setTitle("1 Tag Search");
                builder.setMessage("Enter desired tag.");

                View customLayout = getLayoutInflater().inflate(R.layout.tag1search, null);
                builder.setView(customLayout);

                // Set up the buttons
                builder.setPositiveButton("Search", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String tagType = ((Spinner)customLayout.findViewById(R.id.spinner1)).getSelectedItem().toString();
                        String tagValue = ((EditText)customLayout.findViewById(R.id.tagval1)).getText().toString();

                        searchResults.clear();
                        for(Photo photo : userPhotos){
                            HashMap<String, ArrayList<String>> tagMap = photo.getTags();
                            for(Map.Entry<String, ArrayList<String>> e : tagMap.entrySet()){
                                if(e.getKey().equals(tagType)){ // check tagName
                                    for(String tag : e.getValue()){
                                        if(tag.indexOf(tagValue) > -1){ // check tagValue
                                            if(!searchResults.contains(photo)){
                                                searchResults.add(photo);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        photoAdapter.notifyDataSetChanged();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                final AlertDialog popup = builder.create();
                popup.show();
            }
        });

        search2button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                AlertDialog.Builder builder = new AlertDialog.Builder(search2button.getContext());
                builder.setTitle("2 Tag Search");
                builder.setMessage("Enter desired tags.");
                View customLayout = getLayoutInflater().inflate(R.layout.tag2search, null);
                builder.setView(customLayout);

                // Set up the buttons
                builder.setPositiveButton("Search", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // get values
                        // key value pair 1
                        String tagType1 = ((Spinner)customLayout.findViewById(R.id.spinner1)).getSelectedItem().toString();
                        String tagValue1 = ((EditText)customLayout.findViewById(R.id.tagval1)).getText().toString();

                        // and/or
                        RadioGroup rg = customLayout.findViewById(R.id.radiogroup);
                        int radioButtonID = rg.getCheckedRadioButtonId();
                        View radioButton = rg.findViewById(radioButtonID);
                        int index = rg.indexOfChild(radioButton);
                        String conjunction = ((RadioButton)rg.getChildAt(index)).getText().toString();

                        // key/val pair 2
                        String tagType2 = ((Spinner)customLayout.findViewById(R.id.spinner2)).getSelectedItem().toString();
                        String tagValue2 = ((EditText)customLayout.findViewById(R.id.tagval2)).getText().toString();

                        int numMatches = 1;
                        boolean isAdd = false;
                        if(conjunction.equals("And")){
                            isAdd = true;
                            numMatches = 2;
                        }

                        searchResults.clear();
                        for(Photo photo : userPhotos){
                            HashMap<String, ArrayList<String>> tagMap = photo.getTags();
                            for(Map.Entry<String, ArrayList<String>> e : tagMap.entrySet()){
                                if(e.getKey().equals(tagType1)){ // check tagName1
                                    for(String tag : e.getValue()){
                                        if(tag.indexOf(tagValue1) > -1){ // check tagValue1
                                            numMatches--;
                                        }
                                    }
                                }
                                if(e.getKey().equals(tagType2)){ // check tagName2
                                    for(String tag : e.getValue()){
                                        if(tag.indexOf(tagValue2) > -1){ // check tagValue2
                                            numMatches--;
                                        }
                                    }
                                }
                            }
                            if(numMatches <= 0){
                                if(!isDuplicatePhoto(photo, searchResults)){
                                    searchResults.add(photo);
                                }
                                if(isAdd){
                                    numMatches = 2;
                                }else{
                                    numMatches = 1;
                                }

                            }
                        }
                        photoAdapter.notifyDataSetChanged();
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                final AlertDialog popup = builder.create();
                popup.show();
            }
        });
    }

    private void loadUserPhotos(){
        ArrayList<Album> albums = (ArrayList<Album>) user.getAlbumList();
        for(Album a : albums){
            for(Photo p : a.getPics()){
                userPhotos.add(p);
            }
        }
    }

    private boolean isDuplicatePhoto(Photo photo, ArrayList<Photo> moveToAlbumPics){
        for(Photo p : moveToAlbumPics){
            if (photo.getPath().equals(p.getPath())){
                return true;
            }
        }
        return false;
    }
}
