package com.example.amulyamummaneni.androidphotos.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.amulyamummaneni.androidphotos.R;
import com.example.amulyamummaneni.androidphotos.model.Album;
import com.example.amulyamummaneni.androidphotos.model.Photo;
import com.example.amulyamummaneni.androidphotos.model.User;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class ShowAlbum extends AppCompatActivity {

    GridView gridview;
    PhotoAdapter photoAdapter;

    private User user;
    private Album albumObj;

    private Button displayButton;
    private Button addButton;
    private Button removeButton;
    private Button moveButton;

    private Photo currentPhoto;

    Uri imageUri;
    ImageView i;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.album_open);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // get the name and detail from bundle
        Bundle bundle = getIntent().getExtras();
        user = (User)getIntent().getSerializableExtra("User");
        String albumName = bundle.getString(AlbumAdapter.ALBUM_NAME);
        albumObj = (Album)bundle.getSerializable(AlbumAdapter.ALBUM_OBJ);

        //toolbar.setTitle(albumObj.getAlbumName()); // TODO not working?

        // get the name and detail view objects
        TextView albumNameView = (TextView)findViewById(R.id.album_title);

        // set name and detail on the views
        albumNameView.setText(albumName);

        photoAdapter = new PhotoAdapter(getApplicationContext(), albumObj.getPics());
        gridview = findViewById(R.id.gridview);
        gridview.setAdapter(photoAdapter);

        displayButton = (Button)findViewById(R.id.displayButton);
        addButton = (Button)findViewById(R.id.addButton);
        removeButton = (Button)findViewById(R.id.removeButton);
        moveButton = (Button)findViewById(R.id.moveButton);

        addButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                AlertDialog.Builder builder = new AlertDialog.Builder(moveButton.getContext());
                builder.setTitle("Add Photo");
                builder.setMessage("Choose a photo to upload.");

                i = new ImageView(addButton.getContext());
                builder.setView(i);
                builder.setNeutralButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent photoPickerIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        photoPickerIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                        photoPickerIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        photoPickerIntent.setType("image/*");
                        startActivityForResult(photoPickerIntent, 100);
                    }
                });

                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                final AlertDialog popup = builder.create();
                popup.show();
            }
        });

        moveButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){


                String message = "";
                boolean errorExists = false;

                ArrayList<Photo> selectedPhotos = new ArrayList<>();

                for(Photo p: albumObj.getPics()){
                    if(p.getSelected() == true){
                        selectedPhotos.add(p);
                    }
                }

                if(selectedPhotos.size()==0){
                    message = "No photo currently selected. A photo must be selected to perform this action.";
                    errorExists = true;
                }

                if(user.getAlbumList().size() == 1){ // no other album exists to move photo to
                    message = "No other album exists to move photo to.";
                    errorExists = true;
                }

                if(errorExists){
                    AlertDialog.Builder error = new AlertDialog.Builder(moveButton.getContext());
                    error.setTitle("Illegal Action");
                    error.setMessage(message);
                    error.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) { dialog.cancel(); }
                    });
                    error.show();
                    return;
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(moveButton.getContext());
                builder.setTitle("Move Photo");
                builder.setMessage("Choose a destination album.");

                Spinner dropdown = new Spinner(moveButton.getContext());
                dropdown.setPrompt("Select album");
                ArrayList<String> dests = new ArrayList<>();
                for(Album a : user.getAlbumList()){ // add all albums except current album
                    if(!a.getAlbumName().equals(albumObj.getAlbumName())){
                        dests.add(a.getAlbumName());
                    }
                }
                ArrayAdapter<String> menuAdapter = new ArrayAdapter<String>(moveButton.getContext(), R.layout.spinner, dests);
                dropdown.setAdapter(menuAdapter);
                dropdown.setGravity(Gravity.CENTER_HORIZONTAL);
                builder.setView(dropdown);

                // Set up the buttons
                builder.setPositiveButton("Move", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String moveToAlbum = dropdown.getSelectedItem().toString();

                        // remove photo from current album, and add to new album
                        for(Album album : user.getAlbumList()){
                            if(album.getAlbumName().equals(moveToAlbum)){
                                ArrayList<Photo> moveToAlbumPics = album.getPics();
                                for(Photo currPhoto: selectedPhotos) {
                                    if (isDuplicatePhoto(currPhoto, moveToAlbumPics)) { // if photo already in album
                                        AlertDialog.Builder error = new AlertDialog.Builder(builder.getContext());
                                        error.setTitle("Duplicate Detected");
                                        error.setMessage("Photo already exists in this album. Please choose a new destination.");
                                        error.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        });
                                        error.show();
                                        return;
                                    } else {
                                        deletePhoto(currPhoto);
                                        album.getPics().add(currPhoto);
                                        // update user info
                                        Utils.saveObject(getApplicationContext(), user);
                                        return;
                                    }
                                }
                            }
                        }
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                final AlertDialog popup = builder.create();
                popup.show();
            }
        });

        removeButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                ArrayList<Photo> selectedPhotos = new ArrayList<>();

                for(Photo p: albumObj.getPics()){
                    if(p.getSelected() == true){
                        selectedPhotos.add(p);
                    }
                }
                if(selectedPhotos.size()==0){
                    AlertDialog.Builder error = new AlertDialog.Builder(moveButton.getContext());
                    error.setTitle("Illegal Action");
                    error.setMessage("No photo currently selected. A photo must be selected to perform this action.");
                    error.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) { dialog.cancel(); }
                    });
                    error.show();
                    return;
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(removeButton.getContext());
                builder.setTitle("Confirm Album Deletion");
                builder.setMessage("Are you sure you want to delete this photo?");

                // Set up the buttons
                builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        for(Photo p: selectedPhotos){
                            deletePhoto(p);
                        }
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                final AlertDialog popup = builder.create();
                popup.show();

            }
        });

        displayButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                ArrayList<Integer> selectedPhotosIndex = new ArrayList<>();
                for(int i=0; i<albumObj.getPics().size(); i++){
                    if(albumObj.getPics().get(i).getSelected()){
                        selectedPhotosIndex.add(i);
                    }
                }

                if(selectedPhotosIndex.size() == 0){
                    AlertDialog.Builder error = new AlertDialog.Builder(moveButton.getContext());
                    error.setTitle("Illegal Action");
                    error.setMessage("No photo currently selected. A photo must be selected to perform this action.");
                    error.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) { dialog.cancel(); }
                    });
                    error.show();
                    return;
                }

                Bundle bundle1 = new Bundle();
                Intent intent1 = new Intent(getApplicationContext(), ShowPhoto.class);
                intent1.putExtra("User", user);
                intent1.putExtra("PhotoIndex", selectedPhotosIndex);
                intent1.putExtra("Album", albumObj);
                intent1.putExtra("Photo", currentPhoto);
                intent1.putExtras(bundle1);
                ShowAlbum.this.startActivity(intent1);
            }
        });

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Photo currPhoto = albumObj.getPics().get(i);
                currPhoto.setSelected();
                currentPhoto = currPhoto;
                photoAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 100 && resultCode == RESULT_OK){
            imageUri = data.getData();
            try{
                InputStream imageStream = getContentResolver().openInputStream(imageUri);
                Photo p = new Photo(imageUri);
                albumObj.getPics().add(p);
                Utils.saveObject(getApplicationContext(), user);
                photoAdapter.notifyDataSetChanged();
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    // helpers
    private Bitmap decodeUri(Uri selectedImage) throws FileNotFoundException {

        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        //System.out.println("Look at URI: "+selectedImage);
        BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o);
        final int REQUIRED_SIZE = 140;

        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE
                    || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o2);

    }

    private boolean isDuplicatePhoto(Photo photo, ArrayList<Photo> moveToAlbumPics){
        for(Photo p : moveToAlbumPics){
            if (photo.getPath().equals(p.getPath())){
                return true;
            }
        }
        return false;
    }

    private void deletePhoto(Photo photo) {
        // remove from adapter list
        photoAdapter.remove(photo);

        // update view
        photoAdapter.notifyDataSetChanged();

        // remove from album object
        albumObj.getPics().remove(photo);

        // update user info
        Utils.saveObject(getApplicationContext(), user);
    }
}
