package com.example.amulyamummaneni.androidphotos.activity;

import android.content.Context;

import com.example.amulyamummaneni.androidphotos.model.Album;
import com.example.amulyamummaneni.androidphotos.model.User;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class Utils {

    public static void updateUser(User user){
        ArrayList<Album> updatedList = new ArrayList<>();
        for(Album a : user.getAlbumList()){
            updatedList.add(a);
        }
        user.setAlbumList(updatedList);
        System.out.println("in update user: user updated");
    }

    public static void saveObject(Context context, User user){
        updateUser(user);
        File file = new File(context.getFilesDir(), "user.txt");
        if(file.exists()){
            file.delete();
        }
        try {
            file.createNewFile();
            System.out.println("in saveObject; new file created. filepath: "+file.getAbsoluteFile().toString());
            //writing serialized object (create user file if does not exist)
            FileOutputStream fileStream = new FileOutputStream(file); //open file for saving
            ObjectOutputStream out = new ObjectOutputStream(fileStream); //write object into file
            out.writeObject(user); // saves user object into file
            out.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static boolean isDuplicateAlbum(String album, ArrayList<Album> album_list){
        for(Album a : album_list){
            if(a.getAlbumName().equals(album)){
                return true;
            }
        }
        return false;
    }
}
