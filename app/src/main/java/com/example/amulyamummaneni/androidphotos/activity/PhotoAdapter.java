package com.example.amulyamummaneni.androidphotos.activity;

import android.content.Context;
import android.graphics.Color;
import android.media.Image;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.amulyamummaneni.androidphotos.R;
import com.example.amulyamummaneni.androidphotos.model.Photo;
import com.example.amulyamummaneni.androidphotos.model.User;

import java.io.File;
import java.util.ArrayList;

public class PhotoAdapter extends ArrayAdapter<Photo> {

    public PhotoAdapter(Context context, ArrayList<Photo> photoList){
        super(context, R.layout.grid_photo, photoList);
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(R.layout.grid_photo, null);

        // TODO is this necessary to display?
        String[] items = getItem(position).getPath().split("/");
        String caption = items[items.length - 1];
        TextView captionText = (TextView)view.findViewById(R.id.photo_text);
        captionText.setText(caption);
        ImageView imageView = (ImageView) view.findViewById(R.id.photo_image);
        imageView.setImageURI(getItem(position).getUri());

        if(getItem(position).getSelected()){
            captionText.setBackgroundColor(Color.BLUE);
        }else{
            captionText.setBackgroundColor(Color.TRANSPARENT);
        }

//        int draw = getContext().getResources().getIdentifier(getItem(position).getPath(), "drawable", getContext().getPackageName());
//        photo_image.setImageResource(draw);

        return view;
    }
}
