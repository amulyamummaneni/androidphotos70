package com.example.amulyamummaneni.androidphotos.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.example.amulyamummaneni.androidphotos.R;
import com.example.amulyamummaneni.androidphotos.model.Album;
import com.example.amulyamummaneni.androidphotos.model.User;

import java.util.ArrayList;

public class AlbumAdapter extends ArrayAdapter<Album> {

    private User user;

    public static final String ALBUM_NAME = "album_name";
    public static final String ALBUM_OBJ = "album_object";

    public AlbumAdapter(Context context, ArrayList<Album> albumList, User user){
        super(context, R.layout.album, albumList);
        this.user = user;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(R.layout.album, null);

        String album_name = getItem(position).getAlbumName();
        TextView albumNameText = (TextView)view.findViewById(R.id.album_name);
        albumNameText.setText(album_name);

        albumNameText.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Bundle bundle = new Bundle();
                bundle.putString(ALBUM_NAME, getItem(position).getAlbumName()); // current album name
                bundle.putSerializable(ALBUM_OBJ, getItem(position)); // album list
                Intent intent = new Intent(getContext(), ShowAlbum.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("User", user);
                intent.putExtra("Album Index", position);
                intent.putExtras(bundle);
                getContext().startActivity(intent);
            }
        });

        ImageButton renameButton = (ImageButton)view.findViewById(R.id.rename_button);
        renameButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                AlertDialog.Builder builder = new AlertDialog.Builder(renameButton.getContext());
                builder.setTitle("Rename Album");

                // Set up the input
                EditText input = new EditText(renameButton.getContext());
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                input.setText(getItem(position).getAlbumName()); // set text to existing name
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton("Rename", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // get new album name from dialog
                        String newAlbumName = input.getText().toString();

                        if(Utils.isDuplicateAlbum(newAlbumName, (ArrayList) user.getAlbumList())){
                            AlertDialog.Builder error = new AlertDialog.Builder(builder.getContext());
                            error.setTitle("Duplicate Detected");
                            error.setMessage("Album with this name already exists. Please choose a new name.");
                            error.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) { dialog.cancel(); }
                            });
                            error.show();
                        }else{
                            // save to user file
                            Album a = getItem(position);
                            a.setName(newAlbumName);
                            Utils.saveObject(renameButton.getContext(), user);

                            // update listview
                            notifyDataSetChanged();
                        }
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                final AlertDialog popup = builder.create();
                popup.show();
            }
        });

        ImageButton deleteButton = (ImageButton)view.findViewById(R.id.delete_button);
        deleteButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                AlertDialog.Builder builder = new AlertDialog.Builder(deleteButton.getContext());
                builder.setTitle("Confirm Album Deletion");
                builder.setMessage("Are you sure you want to delete this album?");

                // Set up the buttons
                builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Album toDelete = getItem(position);
                        remove(toDelete); // not sure if this works??
                        notifyDataSetChanged();
                        user.getAlbumList().remove(toDelete);
                        Utils.saveObject(deleteButton.getContext(), user);
                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                final AlertDialog popup = builder.create();
                popup.show();
            }
        });

        return view;
    }

}
