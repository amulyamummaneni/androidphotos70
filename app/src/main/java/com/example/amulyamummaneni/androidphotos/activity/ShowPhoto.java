package com.example.amulyamummaneni.androidphotos.activity;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;

import com.example.amulyamummaneni.androidphotos.R;
import com.example.amulyamummaneni.androidphotos.model.Album;
import com.example.amulyamummaneni.androidphotos.model.Photo;
import com.example.amulyamummaneni.androidphotos.model.User;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class ShowPhoto extends AppCompatActivity {
    private Button nextButton;
    private Button prevButton;
    private Bitmap currentImage;
    private ImageView imageView;
    private ListView listView;

    private int index;

    private User user;
    private Album currAlbum;
    private ArrayList<Integer> photoIndex;
    private HashMap<String,ArrayList<String>> tags;
    private ArrayList<String> tagList;
    private ArrayAdapter<String> adapter;
    private Button addTag;
    private Photo currentPhoto;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display);
        nextButton = findViewById(R.id.rightPhoto);
        prevButton = findViewById(R.id.leftPhoto);
        imageView = findViewById(R.id.slideShowImageView);
        listView = findViewById(R.id.tag_list);
        addTag = findViewById(R.id.addTag);
        tagList = new ArrayList<>();
        index = 0; // TEMP
        user = (User)getIntent().getSerializableExtra("User");
        currentPhoto = (Photo)getIntent().getSerializableExtra("Photo");
        currAlbum = (Album)getIntent().getSerializableExtra("Album");
        photoIndex = (ArrayList<Integer>) getIntent().getSerializableExtra("PhotoIndex");
        this.imageView.setImageURI(currAlbum.getPics().get(photoIndex.get(0)).getUri());

        adapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_list_item_1,
                 tagList);
        listView.setAdapter(adapter);
        displayTags();
    }
    private void displayTags(){
        adapter.clear();
        if(index < currAlbum.getPics().size() && currAlbum.getPics().get(photoIndex.get(index)).getTags() !=null) {
            for (String key : currAlbum.getPics().get(photoIndex.get(index)).getTags().keySet()) {
                for (String v : currAlbum.getPics().get(photoIndex.get(index)).getTags().get(key)) {
                    tagList.add(v);
                }
            }
            adapter.notifyDataSetChanged();
        }
    }
    public void onClickAddTag(View view){
        System.out.println("HERE");
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Title");
        View customLayout = getLayoutInflater().inflate(R.layout.tag1search, null);
        builder.setView(R.layout.tag1search);

// Set up the buttons
        builder.setPositiveButton("Save Tag", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(currAlbum.getPics().get(photoIndex.get(index)).getTags() == null){
                    currAlbum.getPics().get(photoIndex.get(index)).setTags(new HashMap<>());
                }

                String tagType = ((Spinner)customLayout.findViewById(R.id.spinner1)).getSelectedItem().toString();
                String tagValue = ((EditText)customLayout.findViewById(R.id.tagval1)).getText().toString();

                HashMap<String, ArrayList<String>> photoTags = currentPhoto.getTags();

                if(tagType.toLowerCase().equals("location")){
                    if(photoTags.containsKey(tagType)){
                        android.app.AlertDialog.Builder error = new android.app.AlertDialog.Builder(getApplicationContext());
                        error.setTitle("Illegal Action");
                        error.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) { dialog.cancel(); }
                        });
                        error.show();
                        return;
                    }
                }

                // add tag to photo hashmap
                ArrayList<String> value = photoTags.get(tagType);
                if(value == null){
                    value = new ArrayList<>();
                }
                value.add(tagValue);
                photoTags.put(tagType, value);


            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    public void nextButton(View view){
        if(index+1 >= photoIndex.size()){
            index = 0;
        }else{
            index++;
        }
        displayTags();
        this.imageView.setImageURI(currAlbum.getPics().get(photoIndex.get(index)).getUri());
        System.out.println("INDEX:"+index);
    }

    public void prevButton(View view){
        if(index-1 < 0){
            index =photoIndex.size()-1;
        }else{
            index--;
        }
        displayTags();
        this.imageView.setImageURI(currAlbum.getPics().get(photoIndex.get(index)).getUri());
        System.out.println("INDEX:"+index);
    }

//    private void displayImage(){
//        try {
//            File imagePath = new File(getFilesDir(), "images");
//            File newFile = new File(imagePath, "sill.jpg");
//            Uri uri = Uri.fromFile(newFile);
//            currentImage = getBitmap(uri);
//            if(currentImage !=null) {
//                imageView.setImageBitmap(currentImage);
//            }else{
//              System.out.println("Image not found");
//            }
//        }
//        catch (Exception e){
//            e.printStackTrace();
//        }
//    }

//    private Bitmap getBitmap(Uri selectedImage) throws FileNotFoundException {
//
//        BitmapFactory.Options o = new BitmapFactory.Options();
//        o.inJustDecodeBounds = true;
//
//
//        BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o);
//        final int REQUIRED_SIZE = 300;
//
//        int width_tmp = o.outWidth, height_tmp = o.outHeight;
//        int scale = 1;
//        while (true) {
//            if (width_tmp / 2 < REQUIRED_SIZE
//                    || height_tmp / 2 < REQUIRED_SIZE) {
//                break;
//            }
//            width_tmp /= 2;
//            height_tmp /= 2;
//            scale *= 2;
//        }
//
//        BitmapFactory.Options o2 = new BitmapFactory.Options();
//        o2.inSampleSize = scale;
//        return BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o2);
//
//    }
//

}
