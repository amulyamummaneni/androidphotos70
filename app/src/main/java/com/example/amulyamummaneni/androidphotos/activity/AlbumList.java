package com.example.amulyamummaneni.androidphotos.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.amulyamummaneni.androidphotos.R;
import com.example.amulyamummaneni.androidphotos.model.Album;
import com.example.amulyamummaneni.androidphotos.model.User;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;


import androidx.appcompat.app.AppCompatActivity;

public class AlbumList extends AppCompatActivity {

    private Button addButton;
    private ListView listView;
    private AlbumAdapter adapter;

    private User user;
    ArrayList<String> album_names;
    ArrayList<Album> album_list;

    public static final String ALBUM_NAME = "album_name";
    public static final String ALBUM_OBJ = "album_object";

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photos_home);

        listView = findViewById(R.id.album_list);
        addButton = findViewById(R.id.addButton);

        createLoadUser(); // load user info from file

        album_names = loadNames();
        if(album_names !=null) {
            for (String name : album_names) {
                System.out.println(name);
            }
        }
        //  initialized album object list
        album_list = user!= null ? (ArrayList)user.getAlbumList() : new ArrayList<>();

        //adapter = new ArrayAdapter<>(this, R.layout.album, album_names);
        adapter = new AlbumAdapter(getBaseContext(), album_list, user);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(
                (p,v,pos,id) -> showAlbum(pos)
        );

    }

    private void showAlbum(int pos) {
        Bundle bundle = new Bundle();
        bundle.putString(ALBUM_NAME,album_names.get(pos)); // current album name
        bundle.putSerializable(ALBUM_OBJ, album_list.get(pos)); // album
        //bundle.putInt("ALBUM_INDEX", album_index);
        Intent intent = new Intent(getApplicationContext(), ShowAlbum.class);
        intent.putExtra("User", user);
        intent.putExtra("Album Index", pos);

        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void handleAddButton(View view){

        AlertDialog.Builder builder = new AlertDialog.Builder(addButton.getContext());
        builder.setTitle("Add New Album");

        // Set up the input
        EditText input = new EditText(addButton.getContext());
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                // get new album name from dialog
                String newAlbumName = input.getText().toString();

                if(Utils.isDuplicateAlbum(newAlbumName, (ArrayList) user.getAlbumList())){
                    AlertDialog.Builder error = new AlertDialog.Builder(builder.getContext());
                    error.setTitle("Duplicate Detected");
                    error.setMessage("Album with this name already exists. Please choose a new name.");
                    error.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) { dialog.cancel(); }
                    });
                    error.show();
                }else{
                    // save to user file
                    Album a = new Album("user", newAlbumName);
                    user.getAlbumList().add(a);
                    Utils.saveObject(getBaseContext(), user);

                    // update listview
                    album_list.add(a);
                    adapter.notifyDataSetChanged();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        final AlertDialog popup = builder.create();
        popup.show();

    }

    public void handleSearchButton(View view){
        Bundle bundle = new Bundle();
        Intent intent = new Intent(getApplicationContext(), SearchActivity.class);
        intent.putExtra("User", user);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    private ArrayList<String> loadNames(){

        ArrayList<String> result = new ArrayList<>();
        if(user!=null) {
            for (Album a : user.getAlbumList()) {
                result.add(a.getAlbumName());
            }
        }
        return result;

    }

    /**
     * Creates file for user's serialized information (albums and photos)
     * */
    private void createLoadUser() {
        try {
            File file = new File(this.getFilesDir(), "user.txt");
            if (!file.exists()) {
                file.createNewFile();
                //writing serialized object (create user file if does not exist)
                FileOutputStream fileStream = new FileOutputStream(file); //open file for saving
                ObjectOutputStream out = new ObjectOutputStream(fileStream); //write object into file
                this.user = new User("user");
                out.writeObject(user); // saves user object into file
                System.out.println("User object created");
                out.close();
            }
            //loading serialized object
            FileInputStream fileInputStream = new FileInputStream(file); //open file for reading
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream); //read object from file
            if (fileInputStream.available() > 0) {
                user = (User) objectInputStream.readObject(); //load object from file to object User
            }
            objectInputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            user = new User("user");
        } catch (IOException e) {
            e.printStackTrace();
            user = new User("user");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            user = new User("user");
        }
    }
}
